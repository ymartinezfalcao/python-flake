{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = pkgs.mkShell {
          LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib.outPath}/lib";
          buildInputs = with pkgs; [
            stdenv.cc.cc.lib
            poetry
            python311Packages.poetry-dynamic-versioning
            mysql80
            pkg-config
            icu
            libxml2
            libxslt
            zstd.dev
            python311Packages.venvShellHook
            rdkafka
            postgresql
            uv
            # pyright
            libmysqlclient
            docker-compose
            nodePackages.bash-language-server
          ];
          venvDir = ".venv";
          shellHook = ''
            python3.11 -m venv .venv
            source $venvDir/bin/activate
            # check if basedpyright binary is installed
            if [[ ! -f $venvDir/bin/basedpyright ]]; then
              pip install basedpyright
            fi
            if [[ ! -f $venvDir/bin/debugpy ]]; then
              pip install debugpy
            fi
            if [[ ! -f $venvDir/bin/pylint ]]; then
              pip install pylint
            fi
            poetry install
          '';
        };
      });
}
