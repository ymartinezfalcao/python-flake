# Python flake for development

Very simple flake to use with direnv and nix to spin up a development environment.

Simply clone this project, and then create a `.envrc` file pointing to it to use with direnv.

``` sh
use flake /path/to/python-flake
```

